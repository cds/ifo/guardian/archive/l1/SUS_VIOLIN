from guardian import GuardState
from guardian import NodeManager
from time import sleep
import subprocess
import time
import os
import cdsutils

# v1 - S. Aston - 04/01/2019
# Script for LLO to manage SUS violin modes

# v2 - S. Aston - 04/03/2019
# Added DAMP ALL and UNDAMP ALL states
# Added some notifications for completing DAMP and UNDAMP all completion and if the overall VM damping switch is off

# v3 - S. Aston - 04/04/2019
# Enabled "goto" states for each specific violin mode order damping or un-damping, but leave damp/undamp all as requestable states only

# v4 - S. Aston - 04/09/2019
# Make almost everything a "goto" state, make "damp_3rd" nominal and remove "damp_all"
# Reduced notify sleeps from 5 to 3 seconds to help expedite things 

# TODO:
# Consider adding some ring-up monitoring and auto shut-off?
# Consider adding some alarms and alerts after reaching a threshold?
# Can we speed up setting up filters by restoring EDB snapshots of violin modes? or using wait=False or confirm=False

# FIXME:
# multiple ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM3', 'ON', wait=False) are broken even using sleep(0.1) between them still only 70%reliable (CDS bug #1139 https://bugzilla.ligo-wa.caltech.edu/bugzilla3/show_bug.cgi?id=1139)

# Define all QUAD suspensions
quads = ['ITMX','ITMY','ETMX','ETMY']

# Define 1st 2nd and 3rd order mode numbers
modes1 = ['3', '4', '5', '6']
modes2 = ['11', '12', '13', '14']
modes3 = ['21', '22', '23', '24']

# Nominal operational state
nominal = 'DAMP_3RD'

# Initial request on initialization
request = 'IDLE'

class INIT(GuardState):
    def main(self):
        return True

class IDLE(GuardState):
    goto = True
    request = True
    
    def main(self):
        return True

class DAMP_1ST(GuardState):
    goto = True
    request = True
    
    def main(self):        
        #if ezca['SUS-ITMX_L2_DAMP_OUT_SW'] == 0:
        #    notify("Violin mode damping switch is disabled")
        for sus in quads:
            for mode in modes1:
                # Clear damping filter histories for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RSET'] = 2
                # Clear monitor filter histories for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_BL_RSET'] = 2
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RMSLP_RSET'] = 2

        # Set ITMX damping filters
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM3', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM10', 'OFF')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM3', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM6', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM10', 'OFF')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM3', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM10', 'OFF')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FM3', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FM10', 'OFF')

        # Set ITMY damping filters
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM3', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM6', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM8', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM10', 'OFF')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM3', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM6', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM8', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM10', 'OFF')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM3', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM6', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM8', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM10', 'OFF')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM3', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM6', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM8', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM10', 'OFF')

        # Set ETMX damping filters
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM3', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM4', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM10', 'OFF')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM1', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM3', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM4', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM7', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM10', 'OFF')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM3', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM4', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM10', 'OFF')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM3', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM4', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM10', 'OFF')

        # Set ETMY damping filters
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM1', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM10', 'OFF')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM1', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM10', 'OFF')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM1', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM10', 'OFF')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM1', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM10', 'OFF')

        # Engage damping filter outputs for all QUADs
        for sus in quads:
            for mode in modes1:
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'ON')

        # Set ITMX damping filter gains
        ezca['SUS-ITMX_L2_DAMP_MODE3_GAIN'] = -8
        ezca['SUS-ITMX_L2_DAMP_MODE4_GAIN'] = 10
        ezca['SUS-ITMX_L2_DAMP_MODE5_GAIN'] = 20
        ezca['SUS-ITMX_L2_DAMP_MODE6_GAIN'] = 0

        # Set ITMY damping filter gains
        ezca['SUS-ITMY_L2_DAMP_MODE3_GAIN'] = -10
        ezca['SUS-ITMY_L2_DAMP_MODE4_GAIN'] = -15
        ezca['SUS-ITMY_L2_DAMP_MODE5_GAIN'] = -10
        ezca['SUS-ITMY_L2_DAMP_MODE6_GAIN'] = -10

        # Set ETMX damping filter gains
        ezca['SUS-ETMX_L2_DAMP_MODE3_GAIN'] = 10
        ezca['SUS-ETMX_L2_DAMP_MODE4_GAIN'] = -10
        ezca['SUS-ETMX_L2_DAMP_MODE5_GAIN'] = 10
        ezca['SUS-ETMX_L2_DAMP_MODE6_GAIN'] = 20

        # Set ETMY damping filter gains
        ezca['SUS-ETMY_L2_DAMP_MODE3_GAIN'] = 10
        ezca['SUS-ETMY_L2_DAMP_MODE4_GAIN'] = -20
        ezca['SUS-ETMY_L2_DAMP_MODE5_GAIN'] = 10
        ezca['SUS-ETMY_L2_DAMP_MODE6_GAIN'] = 10
        
        notify("1st order violin mode damping engaged")
        sleep(3.0)

    def run(self):
        return True

class DAMP_2ND(GuardState):
    goto = True
    request = True

    def main(self):
        #if ezca['SUS-ITMX_L2_DAMP_OUT_SW'] == 0:
        #    notify("Violin mode damping switch is disabled")
        for sus in quads:
            for mode in modes2:
                # Clear damping filter histories for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RSET'] = 2
                # Clear monitor filter histories for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_BL_RSET'] = 2
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RMSLP_RSET'] = 2
                
        # Set ITMX damping filters
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM3', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM8', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE11', 'FM10', 'OFF')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM3', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE12', 'FM10', 'OFF')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM3', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE13', 'FM10', 'OFF')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM3', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE14', 'FM10', 'OFF')

        # Set ITMY damping filters
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM3', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM6', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM8', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE11', 'FM10', 'OFF')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM1', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM3', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM6', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM8', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE12', 'FM10', 'ON')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM3', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM6', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM7', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM8', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE13', 'FM10', 'OFF')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM3', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM6', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM8', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE14', 'FM10', 'OFF')

        # Set ETMX damping filters
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM3', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM4', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE11', 'FM10', 'OFF')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM3', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM4', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE12', 'FM10', 'OFF')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FM3', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FM4', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE13', 'FM10', 'OFF')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM3', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM4', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE14', 'FM10', 'OFF')
        
        # Set ETMY damping filters
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM1', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE11', 'FM10', 'OFF')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM1', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM3', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM4', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE12', 'FM10', 'OFF')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM1', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE13', 'FM10', 'OFF')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM1', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE14', 'FM10', 'OFF')

        # Engage damping filter outputs for all QUADs
        for sus in quads:
            for mode in modes2:
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'ON')

        # Set ITMX damping filter gains
        ezca['SUS-ITMX_L2_DAMP_MODE11_GAIN'] = 100
        ezca['SUS-ITMX_L2_DAMP_MODE12_GAIN'] = -40
        ezca['SUS-ITMX_L2_DAMP_MODE13_GAIN'] = 100
        ezca['SUS-ITMX_L2_DAMP_MODE14_GAIN'] = -100

        # Set ITMY damping filter gains
        ezca['SUS-ITMY_L2_DAMP_MODE11_GAIN'] = -10
        ezca['SUS-ITMY_L2_DAMP_MODE12_GAIN'] = -20
        ezca['SUS-ITMY_L2_DAMP_MODE13_GAIN'] = -20
        ezca['SUS-ITMY_L2_DAMP_MODE14_GAIN'] = 10

        # Set ETMX damping filter gains
        ezca['SUS-ETMX_L2_DAMP_MODE11_GAIN'] = 40
        ezca['SUS-ETMX_L2_DAMP_MODE12_GAIN'] = -20
        ezca['SUS-ETMX_L2_DAMP_MODE13_GAIN'] = -10
        ezca['SUS-ETMX_L2_DAMP_MODE14_GAIN'] = -80

        # Set ETMY damping filter gains
        ezca['SUS-ETMY_L2_DAMP_MODE11_GAIN'] = 20
        ezca['SUS-ETMY_L2_DAMP_MODE12_GAIN'] = -100
        ezca['SUS-ETMY_L2_DAMP_MODE13_GAIN'] = 40
        ezca['SUS-ETMY_L2_DAMP_MODE14_GAIN'] = -100
        
        notify("2nd order violin mode damping engaged")
        sleep(3.0)

    def run(self):
        return True

class DAMP_3RD(GuardState):
    goto = True
    request = True

    def main(self):
        #if ezca['SUS-ITMX_L2_DAMP_OUT_SW'] == 0:
        #    notify("Violin mode damping switch is disabled")
        for sus in quads:
            for mode in modes3:
                # Clear damping filter histories for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RSET'] = 2
                # Clear monitor filter histories for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_BL_RSET'] = 2
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RMSLP_RSET'] = 2
                
        # Set ITMX damping filters
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM3', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM10', 'OFF')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM3', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM10', 'OFF')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM3', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM10', 'OFF')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM1', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM2', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM3', 'ON')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM4', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM5', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM6', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM7', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM8', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM9', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM10', 'OFF')
        
        # Set ITMY damping filters
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM3', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM6', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM8', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM10', 'OFF')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM3', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM6', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM8', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM10', 'OFF')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM3', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM6', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM8', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM10', 'OFF')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM1', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM2', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM3', 'ON')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM4', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM5', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM6', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM7', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM8', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM9', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM10', 'OFF')
        
        # Set ETMX damping filters
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM3', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM4', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM10', 'OFF')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM3', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM4', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM10', 'OFF')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM3', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM4', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM10', 'OFF')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM1', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM2', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM3', 'ON')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM4', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM5', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM6', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM7', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM8', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM9', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM10', 'OFF')
        
        # Set ETMY damping filters
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM1', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM10', 'OFF')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM1', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM10', 'OFF')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM1', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM10', 'OFF')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM1', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM2', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM3', 'ON')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM4', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM5', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM6', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM7', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM8', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM9', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM10', 'OFF')
        
        # Engage damping filter outputs for all QUADs
        for sus in quads:
            for mode in modes3:
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'ON')

        # Set ITMX damping filter gains
        ezca['SUS-ITMX_L2_DAMP_MODE21_GAIN'] = 800
        ezca['SUS-ITMX_L2_DAMP_MODE22_GAIN'] = 1000
        ezca['SUS-ITMX_L2_DAMP_MODE23_GAIN'] = -1000
        ezca['SUS-ITMX_L2_DAMP_MODE24_GAIN'] = -1000
        
        # Set ITMY damping filter gains
        ezca['SUS-ITMY_L2_DAMP_MODE21_GAIN'] = 1000
        ezca['SUS-ITMY_L2_DAMP_MODE22_GAIN'] = 1000
        ezca['SUS-ITMY_L2_DAMP_MODE23_GAIN'] = 1000
        ezca['SUS-ITMY_L2_DAMP_MODE24_GAIN'] = 1000
        
        # Set ETMX damping filter gains
        ezca['SUS-ETMX_L2_DAMP_MODE21_GAIN'] = 2000
        ezca['SUS-ETMX_L2_DAMP_MODE22_GAIN'] = -400 
        ezca['SUS-ETMX_L2_DAMP_MODE23_GAIN'] = 800
        ezca['SUS-ETMX_L2_DAMP_MODE24_GAIN'] = 800
        
        # Set ETMY damping filter gains
        ezca['SUS-ETMY_L2_DAMP_MODE21_GAIN'] = 500
        ezca['SUS-ETMY_L2_DAMP_MODE22_GAIN'] = -1000
        ezca['SUS-ETMY_L2_DAMP_MODE23_GAIN'] = 800
        ezca['SUS-ETMY_L2_DAMP_MODE24_GAIN'] = -1000
        
        notify("3rd order violin mode damping engaged")
        sleep(3.0)

    def run(self):
        return True

class UNDAMP_1ST(GuardState):
    goto = True
    request = True
    
    def main(self):
        for sus in quads:
            for mode in modes1:
                # Zero the gains for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
                # Disengage damping filter outputs for all QUADs
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'OFF')
        notify("1st order violin mode damping disengaged")
        sleep(3.0)

    def run(self):
        return True

class UNDAMP_2ND(GuardState):
    goto = True
    request = True

    def main(self):
        for sus in quads:
            for mode in modes2:
                # Zero the gains for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
                # Disengage damping filter outputs for all QUADs
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'OFF')
        notify("2nd order violin mode damping disengaged")
        sleep(3.0)

    def run(self):
        return True

class UNDAMP_3RD(GuardState):
    goto = True
    request = True
    
    def main(self):
        for sus in quads:
            for mode in modes3:
                # Zero the gains for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
                # Disengage damping filter outputs for all QUADs
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'OFF')
        notify("3rd order violin mode damping disengaged")
        sleep(3.0)

    def run(self):
        return True

class UNDAMP_ALL(GuardState):
    goto = True
    request = True

    def main(self):
        for sus in quads:
            for mode in modes1 + modes2 + modes3:
                # Zero the gains for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
                # Disengage damping filter outputs for all QUADs
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'OFF')
        notify("All violin mode damping disengaged")
        sleep(3.0)

    def run(self):
        return True

edges = [
    ('INIT','IDLE'),
    ('IDLE','DAMP_1ST'),
    ('IDLE','DAMP_2ND'),
    ('IDLE','DAMP_3RD'),
    ('IDLE','UNDAMP_1ST'),
    ('IDLE','UNDAMP_2ND'),
    ('IDLE','UNDAMP_3RD'),
    ('IDLE','UNDAMP_ALL'),
    ]

edges += [('DAMP_1ST','DAMP_1ST')]
edges += [('DAMP_2ND','DAMP_2ND')]
edges += [('DAMP_3RD','DAMP_3RD')]
edges += [('UNDAMP_1ST','UNDAMP_1ST')]
edges += [('UNDAMP_2ND','UNDAMP_2ND')]
edges += [('UNDAMP_3RD','UNDAMP_3RD')]
edges += [('UNDAMP_ALL','UNDAMP_ALL')]
